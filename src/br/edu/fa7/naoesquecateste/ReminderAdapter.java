package br.edu.fa7.naoesquecateste;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.edu.fa7.naoesqueca.model.ContatoLembrete;
import br.edu.fa7.naoesqueca.model.Lembrete;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ReminderAdapter extends ArrayAdapter<ContatoLembrete> {
  private Context mContext;
  private int mResourceId;
  private List<ContatoLembrete> mList;

  public ReminderAdapter(Context context, int resource) {
    super(context, resource);
    mContext = context;
    mResourceId = resource;
    mList = new ArrayList<ContatoLembrete>();
  }
  
  @Override
  public int getCount() {
    return mList.size();
  }
  
  @Override
  public ContatoLembrete getItem(int position) {
    return mList.get(position);
  }
  
  @Override
  public long getItemId(int position) {
    return getItem(position).getId();
  }
  
  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    // TODO Auto-generated method stub
    LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View v = (View)inflater.inflate(R.layout.reminder_adapter, null);
    ContatoLembrete reminder = getItem(position);
    
    TextView tvMsg = (TextView)v.findViewById(R.id.reminder_msg);
    TextView tvDate = (TextView)v.findViewById(R.id.reminder_date_value);
    TextView tvSenderHeader = (TextView)v.findViewById(R.id.reminder_sender_header);
    TextView tvSender = (TextView)v.findViewById(R.id.reminder_sender_value);
    
    if (reminder.getContatoDestino() == null) {
      tvSenderHeader.setText("Sender:");
      tvSender.setText(reminder.getContatoRemetente());
    }
    
    else {
      tvSenderHeader.setText("Destiny:");
      tvSender.setText(reminder.getContatoDestino());
    }
    
    tvMsg.setText(reminder.getLembrete().getConteudo());
    tvDate.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(reminder.getLembrete().getDataHoraEnvio()));
    
    return v; 
  }
  
  public void add(ContatoLembrete object) {
    // TODO: insert in db
    
    mList.add(object);
    notifyDataSetChanged();
  }
  
  public void remove(ContatoLembrete reminder) {
    // TODO: remove from db
    
    mList.remove(reminder);
    notifyDataSetChanged();
  }
  
  public void update(int pos, ContatoLembrete r) {
    r.setId(getItem(pos).getId());
    // TODO: update db
    
    mList.remove(pos);
    mList.add(pos, r);
    notifyDataSetChanged();
  }
  
}
