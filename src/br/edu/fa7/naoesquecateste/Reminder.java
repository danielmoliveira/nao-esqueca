package br.edu.fa7.naoesquecateste;

import java.io.Serializable;
import java.util.Date;

public class Reminder implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private int mId;
  private String mMessage;
  private Date mSentDate;
  private Date mReceivedDate;
  
  public Reminder(int id, String msg, Date sentDate, Date receivedDate) {
    mMessage = msg;
    mId = id;
    mReceivedDate = receivedDate;
    mSentDate = sentDate;
  }
  
  public int getId() {
    return mId;
  }
  public void setId(int mId) {
    this.mId = mId;
  }
  public String getMessage() {
    return mMessage;
  }
  public void setMessage(String mMessage) {
    this.mMessage = mMessage;
  }
  public Date getSentDate() {
    return mSentDate;
  }
  public void setSentDate(Date mSentDate) {
    this.mSentDate = mSentDate;
  }
  public Date getReceivedDate() {
    return mReceivedDate;
  }
  public void setReceivedDate(Date mReceivedDate) {
    this.mReceivedDate = mReceivedDate;
  }
}
