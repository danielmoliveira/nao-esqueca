package br.edu.fa7.naoesquecateste;

import java.util.Date;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;
import br.edu.fa7.naoesqueca.dao.ContatoLembreteDao;
import br.edu.fa7.naoesqueca.dao.LembreteDao;
import br.edu.fa7.naoesqueca.model.ContatoLembrete;
import br.edu.fa7.naoesqueca.model.Lembrete;

public class ReceiveSMS extends BroadcastReceiver {  
	final SmsManager sms = SmsManager.getDefault();
	 private LembreteDao lembretedao;
	  private Lembrete lembrete;
	  
	  private ContatoLembreteDao contatoDao;
	

	@Override
	public void onReceive(Context context, Intent intent) {

		// Retrieves a map of extended data from the intent.
		final Bundle bundle = intent.getExtras();

		try {

			if (bundle != null) {

				final Object[] pdusObj = (Object[]) bundle.get("pdus");

				for (int i = 0; i < pdusObj.length; i++) {

					SmsMessage currentMessage = SmsMessage
							.createFromPdu((byte[]) pdusObj[i]);
					String phoneNumber = currentMessage
							.getDisplayOriginatingAddress();

					String senderNum = phoneNumber;
					String message = currentMessage.getDisplayMessageBody();

					Log.i("SmsReceiver", "senderNum: " + senderNum
							+ "; message: " + message);

					ContatoLembrete cl = messageToContatoLembrete(message);
					
					 lembretedao = new LembreteDao(context);
	          contatoDao = new ContatoLembreteDao(context);
					
					if (cl != null) {
					  cl.setContatoRemetente(senderNum);
					  long id = lembretedao.insert(cl.getLembrete());
					  cl.setIdLembrete(id);
					  contatoDao.insert(cl);
					  ((MainActivity)context).onReminderReceived(cl);
					}
					
					// Show alert
					int duration = Toast.LENGTH_LONG;
					Toast.makeText(
							context,
							"senderNum: " + senderNum + ", message: " + message,
							duration).show();
					

				} // end for loop
			} // bundle is null

		} catch (Exception e) {
			Log.e("SmsReceiver", "Exception smsReceiver" + e);

		}
	}
	
	public static ContatoLembrete messageToContatoLembrete(String message) {
	  ContatoLembrete cl = null;
    // String frase = "NE@13/06/2014@20:00@S@asdasdasdasd";
    //String msg[] = new String[5];

    if (message.substring(0, 3).equals("NE@")) {
      String msg[] = message.split("@");
      
      if (msg.length >= 5) {
        cl = new ContatoLembrete();
        cl.setLembrete(new Lembrete());
        
        cl.getLembrete().setDataHora(msg[1] + " " + msg[2]);
        cl.getLembrete().setDataHoraRecebimento(new Date());
        cl.getLembrete().setDataHoraEnvio(new Date());
        cl.getLembrete().setTipoLembrete(msg[3].charAt(0));
        cl.setAtivo(true);
        String msgLembre = msg[4];
        cl.getLembrete().setConteudo(msgLembre);
      }
    }
    return cl;
	}

}
