package br.edu.fa7.naoesquecateste;

import java.util.Date;

import br.edu.fa7.naoesqueca.dao.ContatoLembreteDao;
import br.edu.fa7.naoesqueca.dao.LembreteDao;
import br.edu.fa7.naoesqueca.model.ContatoLembrete;
import br.edu.fa7.naoesqueca.model.Lembrete;
import android.R.menu;
import android.app.Activity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class RemindersSendFragment extends android.support.v4.app.Fragment {
	private ReminderListener mListener = null;
	
	private Button btnSend;
	private EditText phoneNumber;
	private EditText smsMessage;
	private EditText smsTitle;
	private ToggleButton alarme;
	private EditText dataAlarme;
	private EditText horaAlarme;
	private boolean isAlarme;
	public static final String PREFIXO = "NE@";
	public static final String DELIMITADOR = "@";
	
	private LembreteDao dao;
	private Lembrete lembrete;
	
	private ContatoLembreteDao contatoDao;
	private ContatoLembrete contato;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.fragment_send, container, false);
		
		dao = new LembreteDao(v.getContext());
		contatoDao = new ContatoLembreteDao(v.getContext());
		
		btnSend = (Button) v.findViewById(R.id.buttonSend);
		phoneNumber = (EditText) v.findViewById(R.id.editTextPhoneNo);
		smsMessage = (EditText) v.findViewById(R.id.editTextSMS);
//		smsTitle = (EditText) v.findViewById(R.id.editTitleSMS);
		alarme = (ToggleButton) v.findViewById(R.id.toggleAlarme);
		dataAlarme = (EditText) v.findViewById(R.id.editDataAlarme);
		horaAlarme = (EditText) v.findViewById(R.id.editHoraAlarme);
		
		alarme.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				isAlarme = (alarme).isChecked();
			}
		});
		
		btnSend.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View v) {
			
			lembrete = new Lembrete();
			contato = new ContatoLembrete();
			
			String phoneNo = phoneNumber.getText().toString();
			String message = smsMessage.getText().toString();
//			String title = smsTitle.getText().toString();
			String dataAlarmeTexto = dataAlarme.getText().toString();
			String horaAlarmeTexto = horaAlarme.getText().toString();
			String ativarAlarme = alarmeAtivado(isAlarme);
			
			lembrete.setConteudo(message);
			lembrete.setDataHora(dataAlarmeTexto + " " + horaAlarmeTexto);
//			lembrete.setTitulo(title);
			lembrete.setDataHoraEnvio(new Date());
			lembrete.setTipoLembrete(isAlarme ? 'a' : 'n');
			
			contato.setAtivo(true);
			contato.setContatoRemetente(null);
			contato.setContatoDestino(phoneNo);
			contato.setIdGlobal(null);
			
			
			String sms = montaMensagem(message, dataAlarmeTexto, horaAlarmeTexto, ativarAlarme);

			try {
				SmsManager smsManager = SmsManager.getDefault();
				smsManager.sendTextMessage(phoneNo, null, sms, null, null);
				Toast.makeText(v.getContext(), "SMS Enviado com sucesso!", Toast.LENGTH_LONG).show();
				long id = dao.insert(lembrete);
				contato.setIdLembrete(id);
				contato.setLembrete(lembrete);
				
				contatoDao.insert(contato);
				mListener.onReminderSent(contato);
			} catch (Exception e) {
				Toast.makeText(v.getContext(), "Falha ao enviar SMS, favor tente novamente!", Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}

		}
	});
		
		return v;
	}

	private String alarmeAtivado(boolean alarme) {
		
		if (alarme) {
			return "S";
		} else {
			return "N";
		}
	}
	
	private String montaMensagem(String message, String data, String hora, String alarme) {
		String msg = PREFIXO + 
				data +
				DELIMITADOR +
				hora +
				DELIMITADOR +
				alarme +
				DELIMITADOR +
				message;
		
		return msg;
				
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		try {
			mListener = (ReminderListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement ReminderListener");
		}
	}

}
