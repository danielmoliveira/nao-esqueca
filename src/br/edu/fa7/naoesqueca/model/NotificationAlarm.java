package br.edu.fa7.naoesqueca.model;

import java.io.Serializable;

public class NotificationAlarm implements Serializable{
	private Long id;
	private Long idAndroid;
	private Long ativo;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdAndroid() {
		return idAndroid;
	}
	public void setIdAndroid(Long idAndroid) {
		this.idAndroid = idAndroid;
	}
	public Long getAtivo() {
		return ativo;
	}
	public void setAtivo(Long ativo) {
		this.ativo = ativo;
	}
}
