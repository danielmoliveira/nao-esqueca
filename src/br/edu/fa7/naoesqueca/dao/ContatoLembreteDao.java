package br.edu.fa7.naoesqueca.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.edu.fa7.naoesqueca.model.ContatoLembrete;

public class ContatoLembreteDao {
	private final static String DB_NAME = "nao_esqueca_db";
	private final static int DB_VERSION = 1;
	public final static String TABLE_NAME = "contrato_lembrete";
	private final static String CREATE_TABLE = "CREATE TABLE " + 
			TABLE_NAME + 
			" (id INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ "id_lembrete INTEGER NOT NULL, "
			+ "contato_destino TEXT, "
			+ "contato_remetente TEXT, "
			+ "ativo TEXT NOT NULL, "
			+ "id_global INTEGER, "
			+ "FOREIGN KEY(id_lembrete) REFERENCES lembrete(id))";
	
	private ContatoLembreteSQLHelper mSqlHelper;
	private Context mContext;
	private SQLiteDatabase mDb;
	
	public ContatoLembreteDao(Context c) {
		mContext = c;
		mSqlHelper = new ContatoLembreteSQLHelper(mContext, TABLE_NAME, DB_VERSION, CREATE_TABLE);
		mDb = mSqlHelper.getWritableDatabase();
	}
	
	public long insert(ContatoLembrete cl) {
		ContentValues content = new ContentValues();
		content.put("id_lembrete",  cl.getIdLembrete());
		content.put("contato_destino", cl.getContatoDestino());
		content.put("contato_remetente", cl.getContatoRemetente());
		content.put("ativo", cl.getAtivo());
		content.put("id_global", cl.getIdGlobal());
		return mDb.insert(TABLE_NAME, null, content);
	}
	
	public void update(ContatoLembrete cl) {
		ContentValues content = new ContentValues();
		content.put("id_lembrete",  cl.getIdLembrete());
		content.put("contato_destino", cl.getContatoDestino());
		content.put("contato_remetente", cl.getContatoRemetente());
		content.put("ativo", cl.getAtivo());
		content.put("id_global", cl.getIdGlobal());
		mDb.update(TABLE_NAME, content, "id = ?", new String[] {  cl.getId().toString() });
	}
	
	public void delete(ContatoLembrete cl) {
		mDb.delete(TABLE_NAME, "id = ?", new String[] {  cl.getId().toString() });
	}
	
	public ContatoLembrete query(Integer id) {
		Cursor c = mDb.query(TABLE_NAME, null, "id = ?", new String[] { id.toString() }, null, null, null);
		
		if (c.moveToFirst()) {
			return new ContatoLembrete(c);
		}
		
		return null;
	}
	
	public List<ContatoLembrete> queryAll() {
		Cursor c = mDb.query(TABLE_NAME, null, null, null, null, null, null);
		
		List<ContatoLembrete> list = new ArrayList<ContatoLembrete>();
		while (c.moveToNext()) {
			list.add(new ContatoLembrete(c));
		}
		
		return list;
	}
	
	public List<ContatoLembrete> searchSentReminders(){
		Cursor c = mDb.rawQuery("SELECT * FROM contrato_lembrete WHERE contato_destino is not null", null);
		LembreteDao lembreteDao = new LembreteDao(this.mContext);
		
		List<ContatoLembrete> list = new ArrayList<ContatoLembrete>();
		while (c.moveToNext()) {
			ContatoLembrete cl = new ContatoLembrete(c);
			if(cl.getIdLembrete() != null){
				cl.setLembrete(lembreteDao.query(Integer.valueOf(cl.getIdLembrete().toString())));
				list.add(cl);
			}
		}
		
		return list;
	}
	
	public List<ContatoLembrete> searchReceivedReminders(){
		Cursor c = mDb.rawQuery("SELECT * FROM contrato_lembrete WHERE contato_remetente is not null", null);
		LembreteDao lembreteDao = new LembreteDao(this.mContext);
		
		List<ContatoLembrete> list = new ArrayList<ContatoLembrete>();
		while (c.moveToNext()) {
			ContatoLembrete cl = new ContatoLembrete(c);
			if(cl.getIdLembrete() != null){
				cl.setLembrete(lembreteDao.query(Integer.valueOf(cl.getIdLembrete().toString())));
				list.add(cl);
			}
		}
		
		return list;
	}
}