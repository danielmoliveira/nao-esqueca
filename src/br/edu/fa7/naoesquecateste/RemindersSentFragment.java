package br.edu.fa7.naoesquecateste;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import br.edu.fa7.naoesqueca.model.ContatoLembrete;

public class RemindersSentFragment extends android.support.v4.app.ListFragment {
  private ReminderListener mListener = null;
  
  @Override
  public void onAttach(Activity activity) {
    // TODO Auto-generated method stub
    super.onAttach(activity);
    try {
        mListener = (ReminderListener) activity;
    }
    catch (ClassCastException e) {
        throw new ClassCastException(activity.toString() + " must implement ReminderListener");
    }
  }
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // TODO Auto-generated method stub
    return super.onCreateView(inflater, container, savedInstanceState);
  }
  
  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    // TODO Auto-generated method stub
    super.onActivityCreated(savedInstanceState);
    getListView().setOnItemLongClickListener((OnItemLongClickListener)getActivity());
  }
  
  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
    // Send the event and Uri to the host activity
    ContatoLembrete reminder = (ContatoLembrete)getListAdapter().getItem(position);
    mListener.onReminderSelected(reminder);
  }
}
