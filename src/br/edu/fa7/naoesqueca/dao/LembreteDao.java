package br.edu.fa7.naoesqueca.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.edu.fa7.naoesqueca.model.Lembrete;

public class LembreteDao {
	private final static String DB_NAME = "nao_esqueca_db";
	private final static int DB_VERSION = 1;
	public final static String TABLE_NAME = "lembrete";
	private final static String CREATE_TABLE = "CREATE TABLE " + 
			TABLE_NAME + 
			" (id INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ "conteudo TEXT NOT NULL, "
			+ "tipo_lembrete TEXT, "
			+ "data_hora TEXT, "
			+ "data_hora_envio DATE, "
			+ "data_hora_recebimento DATE, "
			+ "id_notification_alarm INTEGER,"
			+ "titulo TEXT)"; 
	
	private LembreteSQLHelper mSqlHelper;
	private Context mContext;
	private SQLiteDatabase mDb;
	
	public LembreteDao(Context c) {
		mContext = c;
		mSqlHelper = new LembreteSQLHelper(mContext, TABLE_NAME, DB_VERSION, CREATE_TABLE);
		mDb = mSqlHelper.getWritableDatabase();
	}
	
	public long insert(Lembrete l) {
		ContentValues content = new ContentValues();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
		content.put("conteudo",  l.getConteudo());
		content.put("tipo_lembrete", l.getTipoLembrete().toString());
		content.put("data_hora", l.getDataHora());
		content.put("data_hora_envio", sdf.format(l.getDataHoraEnvio()));
		content.put("data_hora_recebimento", l.getDataHoraRecebimento() == null ? null : sdf.format(l.getDataHoraRecebimento()));
		content.put("id_notification_alarm", l.getIdAlarmNotificaton() == null ? 0 : l.getIdAlarmNotificaton());
		content.put("titulo", (l.getTitulo() == null? "": l.getTitulo()));
		return mDb.insert(TABLE_NAME, null, content);
	}
	
	public void update(Lembrete l) {
		ContentValues content = new ContentValues();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
		content.put("conteudo",  l.getConteudo());
		content.put("tipo_lembrete", l.getTipoLembrete().toString());
		content.put("data_hora", l.getDataHora());
		content.put("data_hora_envio", sdf.format(l.getDataHoraEnvio()));
		content.put("data_hora_recebimento", sdf.format(l.getDataHoraRecebimento()));
		content.put("id_notification_alarm", l.getIdAlarmNotificaton() == null ? 0 : l.getIdAlarmNotificaton());
		mDb.update(TABLE_NAME, content, "id = ?", new String[] {  l.getId().toString() });
	}
	
	public void delete(Lembrete l) {
		mDb.delete(TABLE_NAME, "id = ?", new String[] {  l.getId().toString() });
	}
	
	public Lembrete query(Integer id) {
		Cursor c = mDb.query(TABLE_NAME, null, "id = ?", new String[] { id.toString() }, null, null, null);
		
		if (c.moveToFirst()) {
			return new Lembrete(c);
		}
		
		return null;
	}
	
	public List<Lembrete> queryAll() {
		Cursor c = mDb.query(TABLE_NAME, null, null, null, null, null, null);
		
		List<Lembrete> list = new ArrayList<Lembrete>();
		while (c.moveToNext()) {
			list.add(new Lembrete(c));
		}
		
		return list;
	}
}