package br.edu.fa7.naoesquecateste;

import br.edu.fa7.naoesqueca.model.ContatoLembrete;

public interface ReminderListener {
  public void onReminderSelected(ContatoLembrete reminder);
  public void onReminderSent(ContatoLembrete reminder);
  public void onReminderReceived(ContatoLembrete reminder);
  public void onReminderDeleted(ContatoLembrete reminder, Boolean receivedReminder);
}
