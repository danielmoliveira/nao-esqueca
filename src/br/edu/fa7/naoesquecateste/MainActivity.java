package br.edu.fa7.naoesquecateste;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Toast;
import br.edu.fa7.naoesqueca.dao.ContatoLembreteDao;
import br.edu.fa7.naoesqueca.dao.LembreteDao;
import br.edu.fa7.naoesqueca.model.ContatoLembrete;
import br.edu.fa7.naoesqueca.model.Lembrete;

public class MainActivity extends FragmentActivity implements ActionBar.TabListener, OnPageChangeListener, ReminderListener, OnItemLongClickListener {
	
	private ViewPager mViewPager;
	private SectionPagerAdapter mAdapter;
	
	private ActionBar mActionBar;
	private List<Lembrete> lembretes;
	private ContatoLembreteDao dao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		dao = new ContatoLembreteDao(this);
		
		mActionBar = getActionBar();
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mAdapter = new SectionPagerAdapter(getSupportFragmentManager(), getApplicationContext());
		
		mViewPager.setAdapter(mAdapter);
		mViewPager.setOnPageChangeListener(this);
		
		for (int i = 0; i < mAdapter.getCount(); i++) {
		  mActionBar.addTab(mActionBar.newTab().setText(mAdapter.getTitleItem(i)).setTabListener(this));
		}
		
		List<ContatoLembrete> receiveList = dao.searchReceivedReminders();
		List<ContatoLembrete> sentList = dao.searchSentReminders();
		
		if (receiveList != null) {
		  for (ContatoLembrete x : receiveList) {
		    ((ReminderAdapter)mAdapter.getReceivedFragment().getListAdapter()).add(x);
		  }
		}
		
		if (sentList != null) {
      for (ContatoLembrete x : sentList) {
        ((ReminderAdapter)mAdapter.getSentFragment().getListAdapter()).add(x);
      }
    }
	}
	
	@Override
	protected void onResume() {
	  super.onResume();
	  
	  /*Lembrete lembrete = new Lembrete();
    ContatoLembrete contatoLembrete = new ContatoLembrete();
    
    lembrete.setConteudo("CONTEUDOOO");
    lembrete.setDataHora(new Date().toString());
    lembrete.setDataHoraEnvio(new Date());
    lembrete.setDataHoraRecebimento(new Date());
    lembrete.setTipoLembrete('a');
    
    LembreteDao lembreteDao = new LembreteDao(getApplicationContext());
    
    long id = lembreteDao.insert(lembrete);
    
    ContatoLembreteDao clDao = new ContatoLembreteDao(getApplicationContext());
    contatoLembrete.setAtivo(true);
    contatoLembrete.setContatoDestino("contato destino");
    contatoLembrete.setIdLembrete((long)1);
    
    clDao.insert(contatoLembrete);
    
    Toast.makeText(this, "Conteudo: "+ lembreteDao.query(1).getConteudo() + "\n Id Contatolembrete: " + clDao.query(1).getId(), Toast.LENGTH_LONG).show();*/
    
	}

  @Override
  public void onTabReselected(Tab tab, FragmentTransaction ft) {    
  }

  @Override
  public void onTabSelected(Tab tab, FragmentTransaction ft) {
    mViewPager.setCurrentItem(tab.getPosition());
  }

  @Override
  public void onTabUnselected(Tab tab, FragmentTransaction ft) {
  }

  @Override
  public void onReminderSelected(ContatoLembrete reminder) {    
  }

  @Override
  public void onPageScrollStateChanged(int arg0) {
    //Toast.makeText(this, "On Page State Changed Scrolled", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    //Toast.makeText(this, "On Page Scrolled", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onPageSelected(int index) {
    mActionBar.setSelectedNavigationItem(index);
  }

  @Override
  public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
    // TODO: remove from db and from alarm
    Toast.makeText(getApplicationContext(), "TESTE LONG ITEM CLICK", Toast.LENGTH_SHORT).show();
    return false;
  }

  @Override
  public void onReminderSent(ContatoLembrete reminder) {
    ((ReminderAdapter)mAdapter.getSentFragment().getListAdapter()).add(reminder);
  }

  @Override
  public void onReminderReceived(ContatoLembrete reminder) {
    ((ReminderAdapter)mAdapter.getReceivedFragment().getListAdapter()).add(reminder);
  }

  @Override
  public void onReminderDeleted(ContatoLembrete reminder, Boolean receivedReminder) {
    ReminderAdapter rAdapter = ((ReminderAdapter)(receivedReminder? mAdapter.getReceivedFragment(): mAdapter.getSentFragment()).getListAdapter());
  }


	

}
