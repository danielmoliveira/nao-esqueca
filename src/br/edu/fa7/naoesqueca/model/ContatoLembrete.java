package br.edu.fa7.naoesqueca.model;

import java.io.Serializable;
import java.util.Date;

import android.database.Cursor;

public class ContatoLembrete implements Serializable{
	private Long id;
	private Long idLembrete;
	private String contatoDestino;
	private String contatoRemetente;
	private Boolean ativo;
	private Long idGlobal;
	private Lembrete lembrete;
	
	public ContatoLembrete(Long id, Long idLembrete, String contato_destino,
			String contato_remetente, Boolean ativo, Long id_global) {
		super();
		this.id = id;
		this.idLembrete = idLembrete;
		this.contatoDestino = contato_destino;
		this.contatoRemetente = contato_remetente;
		this.ativo = ativo;
		this.idGlobal = id_global;
	}
	
	public ContatoLembrete(Cursor c) {
		this.id = c.getLong(c.getColumnIndex("id"));
		this.idLembrete = c.getLong(c.getColumnIndex("id_lembrete"));
		this.contatoDestino = c.getString(c.getColumnIndex("contato_destino"));
		this.contatoRemetente = c.getString(c.getColumnIndex("contato_remetente"));
		this.ativo = Boolean.valueOf(c.getString(c.getColumnIndex("ativo")));
		this.idGlobal = c.getLong(c.getColumnIndex("id_global"));
	}

	public ContatoLembrete() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdLembrete() {
		return idLembrete;
	}

	public void setIdLembrete(Long idLembrete) {
		this.idLembrete = idLembrete;
	}

	public String getContatoDestino() {
		return contatoDestino;
	}

	public void setContatoDestino(String contatoDestino) {
		this.contatoDestino = contatoDestino;
	}

	public String getContatoRemetente() {
		return contatoRemetente;
	}

	public void setContatoRemetente(String contatoRemetente) {
		this.contatoRemetente = contatoRemetente;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Long getIdGlobal() {
		return idGlobal;
	}

	public void setIdGlobal(Long idGlobal) {
		this.idGlobal = idGlobal;
	}

	public Lembrete getLembrete() {
		return lembrete;
	}

	public void setLembrete(Lembrete lembrete) {
		this.lembrete = lembrete;
	}
	
}
