package br.edu.fa7.naoesqueca.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.database.Cursor;

public class Lembrete implements Serializable {
	private Long id;
	private String conteudo;
	private String titulo;
	private Character tipoLembrete;
	private String dataHora;
	private Date dataHoraEnvio;
	private Date dataHoraRecebimento;
	private Long idAlarmNotificaton;

	public Lembrete(Long id, String conteudo, Character tipoLembrete,
			String dataHora, Date dataHoraEnvio, Date dataHoraRecebimento,
			Long idAlarmNotificaton, String titulo) {
		super();
		this.id = id;
		this.conteudo = conteudo;
		this.tipoLembrete = tipoLembrete;
		this.dataHora = dataHora;
		this.dataHoraEnvio = dataHoraEnvio;
		this.dataHoraRecebimento = dataHoraRecebimento;
		this.idAlarmNotificaton = idAlarmNotificaton;
		this.titulo = titulo;
	}

	public Lembrete(Cursor c) {
		SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy",java.util.Locale.US);
		this.id = c.getLong(c.getColumnIndex("id"));
		this.conteudo = c.getString(c.getColumnIndex("conteudo"));
		this.tipoLembrete = c.getString(c.getColumnIndex("tipo_lembrete"))
				.charAt(0);
		this.dataHora = c.getString(c.getColumnIndex("data_hora"));
		this.dataHoraEnvio = new Date(c.getLong(c
				.getColumnIndex("data_hora_envio")));
		this.dataHoraRecebimento = new Date(c.getLong(c
				.getColumnIndex("data_hora_recebimento")));
		this.idAlarmNotificaton = c.getLong(c
				.getColumnIndex("id_notification_alarm"));
		this.titulo = c.getString(c.getColumnIndex("titulo"));
	}

	public Lembrete() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public Character getTipoLembrete() {
		return tipoLembrete;
	}

	public void setTipoLembrete(Character tipoLembrete) {
		this.tipoLembrete = tipoLembrete;
	}

	public String getDataHora() {
		return dataHora;
	}

	public void setDataHora(String dataHora) {
		this.dataHora = dataHora;
	}

	public Date getDataHoraEnvio() {
		return dataHoraEnvio;
	}

	public void setDataHoraEnvio(Date dataHoraEnvio) {
		this.dataHoraEnvio = dataHoraEnvio;
	}

	public Date getDataHoraRecebimento() {
		return dataHoraRecebimento;
	}

	public void setDataHoraRecebimento(Date dataHoraRecebimento) {
		this.dataHoraRecebimento = dataHoraRecebimento;
	}

	public Long getIdAlarmNotificaton() {
		return idAlarmNotificaton;
	}

	public void setIdAlarmNotificaton(Long idAlarmNotificaton) {
		this.idAlarmNotificaton = idAlarmNotificaton;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
}
