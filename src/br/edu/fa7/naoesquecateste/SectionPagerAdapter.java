package br.edu.fa7.naoesquecateste;

import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;

public class SectionPagerAdapter extends FragmentPagerAdapter {
  private ArrayList<Fragment> mList = new ArrayList<Fragment>();
  private ArrayList<String> mTitleList = new ArrayList<String>();
  RemindersReceivedFragment mReceivedFragment;
  RemindersSentFragment mSentFragment;
  
  public SectionPagerAdapter(android.support.v4.app.FragmentManager fm, Context context) {
    // TODO Auto-generated constructor stub
    super(fm);
    mReceivedFragment = new RemindersReceivedFragment();
    String recTitle = "Received";
    
    mSentFragment = new RemindersSentFragment();
    String sentTitle = "Sent";
    
    RemindersSendFragment sendFragment = new RemindersSendFragment();
    String sendTitle = "Send";
    
    ReminderAdapter receivedAdapter = new ReminderAdapter(context, 0);
    mReceivedFragment.setListAdapter(receivedAdapter);
    
    ReminderAdapter sentAdapter = new ReminderAdapter(context, 1);
    mSentFragment.setListAdapter(sentAdapter);
       
    add(sendTitle, sendFragment);
    add(recTitle, mReceivedFragment);
    add(sentTitle, mSentFragment);
  }
  
  public RemindersReceivedFragment getReceivedFragment() {
    return mReceivedFragment;    
  }
  
  public RemindersSentFragment getSentFragment() {
    return mSentFragment;    
  }
  
  private void add(String title, Fragment fragment) {
    mList.add(fragment);
    mTitleList.add(title);
  }
  
  public String getTitleItem(int pos) {
    return mTitleList.get(pos);
  }
  
  @Override
  public Fragment getItem(int pos) {
    // TODO Auto-generated method stub
    return mList.get(pos);
  }

  @Override
  public int getCount() {
    // TODO Auto-generated method stub
    return mList.size();
  }
}
