package br.edu.fa7.naoesqueca.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ContatoLembreteSQLHelper extends SQLiteOpenHelper {

	private String createSQL;
	
	public ContatoLembreteSQLHelper(Context context, String name, int version, String createSQL) {
		super(context, name, null, version);
		this.createSQL = createSQL;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(createSQL);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE " + ContatoLembreteDao.TABLE_NAME);
		onCreate(db);
	}

}
